<?php
include 'http.php';

$h = new HTTP;
$data_folder = __DIR__ . '/data';
$versions = [];
$versions_page = $h->get('https://www.biblegateway.com/versions/');

if ($versions_page['code'] == 200) {
    preg_match_all('/<tr data-language="([^"]+).*?<\/tr>/s', $versions_page['result'], $rows);

    foreach ($rows[0] as $key => $row) {
        if (empty($versions[$rows[1][$key]])) {
            preg_match('/language-display.*?>([^<]+)/', $row, $name);
            $versions[$rows[1][$key]] = [
                'name' => $name[1],
                'links' => []
            ];
        }

        preg_match('/translation-name.*?href="(.*?)#booklist".*?>(.*?)<\/a>/', $row, $link);

        if (!empty($link[1])) {
            $versions[$rows[1][$key]]['links'][] = [
                'name' => $link[2],
                'link' => "https://www.biblegateway.com{$link[1]}"
            ];
        }
    }

} else {
    echo "Response code is {$versions_page['code']}" . PHP_EOL;
    die;
}

// $versions = include 'data/versions';
echo 'Founded ' . sizeof($versions) . ' versions' . PHP_EOL;

if (!empty($versions)) {
    foreach ($versions as $version) {
        if (!empty($version['links'])) {
            $dir = "{$data_folder}/{$version['name']}";

            if (!file_exists($dir)) {
                mkdir($dir);
                echo "Created {$version['name']}" . PHP_EOL;
            }

            foreach ($version['links'] as $link) {
                $link['name'] = str_replace(['/', '\\'], '_', $link['name']);
                $file = "{$dir}/{$link['name']}.xml";

                if (file_exists($file)) {
                    continue;
                }

                echo "Send request {$link['link']}" . PHP_EOL;
                $index_page = $h->get($link['link']);
                // $index_page = include 'data/version';
                echo "Recieved chapters list of {$link['name']}" . PHP_EOL;

                if ($index_page['code'] == 200) {
                    $parts = [];
                    $xml = new DOMDocument;
                    $xml_root = $xml->createElement('book');
                    $xml->appendChild($xml_root);

                    preg_match_all('/<tr.*?-book.*?<\/tr>/s', $index_page['result'], $index);

                    foreach ($index[0] as $part) {
                        preg_match('/book-name.*?span>([^<]+)/', $part, $name);
                        preg_match_all('/<a.*?href="(.*?)".*?>(.*?)<\/a>/s', $part, $chapters);

                        foreach ($chapters[0] as $key => $chapter) {
                            if (strpos($chapter, 'title')) {
                                preg_match('/title="(.*?)"/', $chapter, $title);
                                $title = $title[1];

                            } else {
                                $title = $chapters[2][$key];
                            }

                            $parts[$name[1]][] = [
                                'name' => $title,
                                'chapter' => $chapters[2][$key],
                                'link' => 'https://www.biblegateway.com' . str_replace('&amp;', '&', $chapters[1][$key])
                            ];
                        }
                    }

                    foreach ($parts as $part_name => $part) {
                        $xml_part = $xml->createElement('part');
                        $xml_part->setAttribute('name', $part_name);
                        $xml_root->appendChild($xml_part);

                        foreach ($part as $chapter) {
                            $xml_chapter = $xml->createElement('chapter');
                            $xml_chapter->setAttribute('name', $chapter['name']);
                            $xml_chapter->setAttribute('number', $chapter['chapter']);
                            $xml_part->appendChild($xml_chapter);

                            echo "Send request {$chapter['link']}" . PHP_EOL;
                            $data = $h->get($chapter['link']);
                            echo "Recieved chapter {$chapter['name']}" . PHP_EOL;
                            // $data = include 'data/page';

                            if ($data['code'] == 200) {
                                if (strpos($data['result'], 'No results found')) {
                                    echo 'No results found' . PHP_EOL;
                                    continue;
                                }

                                preg_match('/<table.*?passage-cols.*?publisher-info-bottom.*?<\/table>/us', $data['result'], $table);

                                if (empty($table)) {
                                    echo 'Table with text was not found, find intro text' . PHP_EOL;

                                    preg_match('/basic-main.*?>\s*(<h2.*?)<\/div>/us', $data['result'], $html);

                                    if (empty($html[0])) {
                                        echo 'Nothing found' . PHP_EOL;
                                        die;
                                    }

                                } else {
                                    preg_match('/text-html.*?(<h1.*?)<\/div>\s*<div.*?publisher-info-bottom/us', $table[0], $html);

                                    if (empty($html[0])) {
                                        echo 'Data was not found' . PHP_EOL;
                                        die;
                                    }
                                }

                                $xml_chapter->appendChild($xml->createCDATASection($html[1]));

                            } else {
                                echo "Response code is {$data['code']}" . PHP_EOL;
                            }
                        }
                    }

                    $xml->save($file);
                    echo 'Bomb has been planted' . PHP_EOL;

                } else {
                    echo "Response code is {$index_page['code']}" . PHP_EOL;
                }
            }
        }
    }
}
