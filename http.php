<?php
class HTTP {
	private $handler;
	private $error = array();

	public function __construct() {
		$this->handler = curl_init();
	}

	public function get($path = null, $params = array()) {
		if (!empty($params['data'])) {
			$params['queryString'] = $this->buildParamsString($params['data']);
			unset($params['data']);
		}

		return $this->request($path, $params);
	}

	public function post($path = null, $params = array()) {
		curl_setopt($this->handler, CURLOPT_POST, true);

		if (!empty($params['data'])) {
			curl_setopt($this->handler, CURLOPT_POSTFIELDS, $params['data']);
		}

		return $this->request($path, $params);
	}

	public function head($path = null, $params = array()) {
		curl_setopt($this->handler, CURLOPT_CUSTOMREQUEST, 'HEAD');
		curl_setopt($this->handler, CURLOPT_HEADER, true);

		if (!empty($params['data'])) {
			$data['queryString'] = $this->buildParamsString($params['data']);
			unset($params['data']);
		}

		return $this->request($path, $params);
	}

	public function put($path = null, $params = array()) {
		curl_setopt($this->handler, CURLOPT_CUSTOMREQUEST, 'PUT');

		if (!empty($params['data'])) {
			curl_setopt($this->handler, CURLOPT_POSTFIELDS, $params['data']);
		}

		return $this->request($path, $params);
	}

	public function delete($path = null, $params = array()) {
		curl_setopt($this->handler, CURLOPT_CUSTOMREQUEST, 'DELETE');

		return $this->request($path, $params);
	}

	public function errorInfo($raw = true) {
		return $raw ? $this->error : sprintf('%s: %s', $this->error['errno'], $this->error['text']);
	}

	private function request($path, $params) {
		if (empty($path)) {
			return false;
		}

		if (!empty($params['queryString'])) {
			$path = implode('?', array($path, $params['queryString']));
		}

		$requestOptions = array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_URL => $path
		);

		if (!empty($params['headers'])) {
			if (!empty($params['headers']['User-Agent'])) {
				$requestOptions[CURLOPT_USERAGENT] = $params['headers']['User-Agent'];
				unset($params['headers']['User-Agent']);
			}

			if (!empty($params['headers']['Referer'])) {
				$requestOptions[CURLOPT_REFERER] = $params['headers']['Referer'];
				unset($params['headers']['Referer']);
			}

			foreach ($params['headers'] as $name => $value) {
				$requestOptions[CURLOPT_HTTPHEADER][] = sprintf('%s: %s', $name, $value);
			}
		}

		if (!empty($params['cookie'])) {
			$requestOptions[CURLOPT_COOKIE] = $this->buildParamsString($params['cookie'], '; ');
		}

		curl_setopt_array($this->handler, $requestOptions);

		$result = curl_exec($this->handler);

		if (curl_errno($this->handler)) {
			$this->error['errno'] = curl_errno($this->handler);
			$this->error['text'] = curl_error($this->handler);

			return false;
		}

		$info = curl_getinfo($this->handler);

		return array(
			'url' => $info['url'],
			'contentType' => $info['content_type'],
			'code' => $info['http_code'],
			'result' => $result
		);
	}

	private function buildParamsString($data = array(), $separator = '&') {
		$queryStringParams = array();

		foreach ($data as $name => $value) {
			$queryStringParams[] = sprintf('%s=%s', $name, $value);
		}

		return implode($separator, $queryStringParams);
	}
}
